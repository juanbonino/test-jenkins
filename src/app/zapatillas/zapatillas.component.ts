import { Component, OnInit } from '@angular/core';
import { Zapatilla } from '../models/zapatilla';

@Component({
    selector: 'zapatillas',
    templateUrl: './zapatillas.component.html'
})

export class ZapatillasComponent implements OnInit{
    public titulo: string = 'Componente de Zapatillas'; 
    public zapatillas: Array<Zapatilla>;
    public marcas: String[];
    public color: string;

    constructor(){
        this.color = 'red';
        this.marcas = new Array();
        this.zapatillas = [
            new Zapatilla('Reebok Classic','Reebok','Blanco',80,true),
            new Zapatilla('Nike Cualquiera','Nike','Rojo',110,true),
            new Zapatilla('Adidas a tu Medida','Adidas','Verde',70,true)
        ];
    }
    ngOnInit(){
        console.log(this.zapatillas);
    }

    getMarcas(){
        this.zapatillas.forEach((zapatilla,index)=>{
            this.marcas.push(zapatilla.marca)
        });
    }

}   